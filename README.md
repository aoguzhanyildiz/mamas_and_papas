# Mamas & Papas

Documentation and setup instructions for the project.

## Overview

- [Techs](#techs)
- [Installation and Running](#installation-and-running)
- [Development](#development)

## Techs

Project built with / based on following techs:

- Docker & Docker-Compose
- RabbitMQ
- Elasticsearch
- Microservice Kit
- Node.js
- React

The project consists of 5 docker containers:
- `frontend`
- `api`
- `search_service`
- `elastic`
- `rabbitmq`.

You can inspect [docker-compose.yml](docker-compose.yml) file for more details on images and containers.

## Installation and Running

### Installation
The only required dependency is `Docker`. This is the docker version of the computer that project was developed:

    $ docker --version
    Docker version 17.09.0-ce, build afdb6d4

`Docker 17.0.9 ` is NOT required BUT recommended. Previous versions of docker might work, but not tested.

To install Docker, you can visit official [web site](https://www.docker.com/community-edition).

### Running

To run the application, first, clone the repository:

    $ git clone https://gitlab.com/aoguzhanyildiz/mamas_and_papas.git

`cd` into project folder:

    $ cd mamas_and_papas

And start it with docker-compose:

    $ docker-compose up

If you don't want to build each container, you can run:

    $ docker-compose up --build

_Note: The `--build` arg is required only for the first time. Unless you don't modify source code of any service, you don't need to `--build` containers every time to run the project._

## Development

If you want to change or update the code, feel free to do it. But once you edited the code, you need to build that service before running. You can either build each container itself, or you can pass an argument to docker-compose command:

    $ docker-compose up --build

This will build all containers before running.
