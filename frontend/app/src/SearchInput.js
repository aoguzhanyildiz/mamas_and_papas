import React from 'react';
import './css/index.css';
import request from 'superagent';
import Dropdown from './Dropdown'


// This is the main component for our app's logic. 
// Server requests, response handling, request canceling. 
// Below, we will see details.
// Stay tuned.
export default class SearchInput extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        keyword: '',
        loading: false,
        data: [],
        typing: false
      }
      this.handleChange = this.handleChange.bind(this);
      this.activeRequest = null;
      this.typingTimeout = null;
  
      // Ok. We are implementing our custom search & dropdown feature.
      // Here is the scenario: 
      // - We type something to search input.
      // - A dropdown came up with results in it.
      // - That dropdown should disappear if we click outside of it. 
      // ** This is what we are doing inside this event listener. **
      document.body.addEventListener('click', e => {
        const isSearchResult = e.target.className === 'search-result';
        const isModalOpen = document.body.classList.contains('ReactModal__Body--open')
        // ignore clicking dropdown itself or modal window.
        if ( isSearchResult || isModalOpen ) return false;
        if (this.state.data.length > 0) {
          this.setState({data: []}); // if this.state.data is empty, dropdown will disappear
        }
      })
    }
  
    // This will be executed whenever user types.
    // Here is how this works:
    // If user starts typing, a timeout (1 seconds) will start.
    // If user continue to typing in 1 second, timeout will be cleared and restart.
    // But if user doesn't type in that 1 second, a request will be sent to the server
    // And we will fill our dropdown with results.
    handleChange(evt) {
      if (this.activeRequest) this.activeRequest.abort(); // if there are any active request, cancel it.
      clearTimeout(this.typingTimeout);;
      let keyword = evt.target.value
      this.setState({keyword: keyword, loading: keyword.length > 2, data: []},  () => {
          this.typingTimeout = setTimeout(() => {
            if (this.state.keyword.length < 3) return;
            this.getDataFromServer()
            this.activeRequest.on('abort', () => console.log('request canceled.'))
          },1000)
        })
    }
  
    // Request the data from server and update the state based on response.
    getDataFromServer = () => {
      this.activeRequest = request
        .get(`http://localhost:5001/search?keyword=${this.state.keyword}`)
        .end((err, resp) => {
          this.activeRequest = null;
          if (err) {
            this.setState({loading: false, data:[]})
          } else {
            this.setState({
              loading: false,
              data: resp.body
            })
          }
        })
    }
  
    render() {
      return(
        <div className="search-area">
          <input type="text" placeholder="Hello, I'm looking for..."
            value={this.state.keyword} onChange={this.handleChange} autoFocus/>
            <span className="search-icon"/>
          {this.state.data.length > 0 || this.state.loading ? <Dropdown loading={this.state.loading} data={this.state.data}/> : null}
        </div>
      )
    }
  }