import React from 'react';
import './css/index.css';
import ReactModal from 'react-modal';
import * as shortid from 'shortid'

// This component will be rendered in <SearchInput/>
// It takes product list as prop and creates a dropdown contains those results
// Additionally, if user clicks one of the results,
// A modal will pop up contains details of the product [sku, ediref, name, stockStatus, description]
export default class Dropdown extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        selectedProduct: null,
        isModalOpen: false
      }
      this.handleCloseModal = this.handleCloseModal.bind(this)
    }
  
    handleCloseModal () {
      this.setState({ isModalOpen: false });
    }
  
    render() {
      let content;
      if (this.props.loading) {
        content = (<div className="search-result">Loading...</div>)
      } else if (this.props.data.length > 0) {
        content = this.props.data.map((result, index) => {
          return (
            // Using loop index as key is not a good practice.
            // Although we could have use `sku` or `ediref`,
            // It is better to use a library generates short unique ids.
            // Like `shortid`. 
            <div key={shortid.generate()} className="search-result"
            onClick={e => {this.setState({selectedProduct: result._source, isModalOpen:true })}}>{result._source.name}</div>
          )
        })
      } else {
        content = (<div className="search-result"></div>)
      }
  
      if (this.state.selectedProduct) {
        return (
          <ReactModal isOpen={this.state.isModalOpen} parentSelector={() => document.body} shouldCloseOnOverlayClick={true} onClick={e => e.preventDefault()}>
            <button onClick={this.handleCloseModal}> Close </button>
            <h4>{this.state.selectedProduct.name}</h4>
            <p><small>sku: </small>{this.state.selectedProduct.sku}</p>
            <p><small>ediRef: </small>{this.state.selectedProduct.ediRef || '-'}</p>
            <p><small>in Stock: </small>
              {this.state.selectedProduct.isInStock ? 
                (<span style={{color: 'green'}}>YES</span>) : 
                (<span style={{color: 'red'}}>NO</span>)
              }
            </p>
            <div style={{borderTop: '1px solid #ddd'}}
            dangerouslySetInnerHTML={{__html: this.state.selectedProduct.description }}/>
          </ReactModal>
        )
      } else {
        return (
          <div className="search-dropdown">
            {content}
          </div>
        )
      }
      
    }
  }