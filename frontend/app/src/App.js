import React, { Component } from 'react';
import './css/index.css';
import Siema from 'siema';
import SearchInput from './SearchInput'

// importing files as modules??
// well, didn't know such thing is possible.
// I think this is something related to webpack's url-loader
import s1 from './s1.jpeg'
import s2 from './s2.jpeg'
import s3 from './s3.jpeg'

// This is main component for our app
class App extends Component {
  constructor(props) {
    super(props)
    this.siema = props.siema;
  }
  componentDidMount(){
    if (this.siema === undefined) {
      this.siema = new Siema({
        selector: '.slide',
        duration: 800,
        easing: 'ease-out',
        perPage: 1,
        startIndex: 0,
        draggable: true,
        loop: true
      })
      // Since we are using one of the simplest image slider, we need to play it
      // by hand. Play next slide every 3 seconds.
      this.interval = setInterval(()=>{this.siema.next()}, 3000)
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <div className="container">
        <header>
          <div className="top-section">
            <div className="grid-cell align-top left">
              <div className="hamburger"/>
            </div>
            <div className="grid-cell">
              <a href="/">
                <div className="logo"></div>
              </a>
            </div>
            <div className="grid-cell align-top right">
                <div className="bag"></div>
                <div className="top-menu">
                  <div className="menu-item">Sign In / Register</div>
                  <div className="menu-item">Stores / Stockists</div>
                  <div className="menu-item">Your Bag (2)</div>
                </div>
            </div>
          </div>
          <div className="bot-section">
            <div className="top-menu-mobile">
                <div className="menu-item">Sign In/Register</div>
                <div className="menu-item">Stores/Stockists</div>
            </div>
            <div className="sub-menu">
              <div className="menu-item">clothing</div>
              <div className="menu-item">travel</div>
              <div className="menu-item">nursery furniture</div>
              <div className="menu-item">nursery interiors</div>
              <div className="menu-item">playtime</div>
              <div className="menu-item">bathtime</div>
              <div className="menu-item">feeding</div>
              <div className="menu-item">gifts</div>
            </div>
          </div>
        </header>
       <SearchInput/>
        <div className="container slide-area">
          <div className="slide">
                <div><img src={s1} alt="1"/></div>
                <div><img src={s2} alt="2"/></div>
                <div><img src={s3} alt="3"/></div>
            </div>
        </div>
      </div>
    );
  }
}

export default App;
