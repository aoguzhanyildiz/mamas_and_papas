const supertest = require('supertest');
var nock = require('nock');

const serverURL = 'http://localhost:5001'

const apiServer = nock(serverURL)
apiServer.get('/search').query(queryObj => {
  if (!queryObj.keyword || queryObj.keyword.length < 3)  return true
}).reply(200, {error: 'Missing Args'})

apiServer.get('/search').query(queryObj => {
  if (!queryObj.keyword || queryObj.keyword.length < 3)  return false
  return true
}).reply(200, {})

apiServer.post(/.*/).reply(405)

const client = supertest(serverURL)

test('get request to /search with KEYWORD ARG was OK.', () => {
   return client.get('/search').query({keyword: 'pushchair'}).expect(200, {})
})

test('get request to /search with KEYWORD with lenth < 3 was NOT OK', () => {
  return client.get('/search').query({keyword: 'pu'}).expect(200, {error: 'Missing Args'})
})

test('post request did return 405 response.', () => {
  return client.post('/search').expect(405)
})
