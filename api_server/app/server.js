const restify = require('restify');
const corsMiddleware = require('restify-cors-middleware')
const MicroserviceKit = require('microservice-kit')

const rabbitmqURL = process.env.RABBITMQ_URL || 'amqp://localhost:5672'

const cors = corsMiddleware({
    origins: [ '*' ]
})

const microserviceKit = new MicroserviceKit({
    type: 'core-worker',
    amqp: {
        url: rabbitmqURL,
        queues: [
            {
                name: "core",
                options: {durable: true},
                key: 'core'
            }
        ],
        exchanges: []
    }
});

const searchResponse = (req, res, next) => {
  const keyword = req.query.keyword
  if (!keyword || keyword.length < 3) {
    res.send({error: 'Missing Args'})
    next()
    return
  }
  const coreQueue = microserviceKit.amqpKit.getQueue('core');
  coreQueue.sendEvent('search', {keyword: keyword}, {persistent: true})
    .then(response => {
        res.send(response.hits.hits)
        next()
    })
    .catch(err => {
        console.log('Negative response: ', err);
        res.send(err)
        next()
    })
}

const server = restify.createServer();
server.pre(cors.preflight)
server.use(cors.actual)
server.use(restify.plugins.queryParser())


server.get('/search', searchResponse);
server.get('/', (req, res, next) => {
  res.send('This is an API Service and nothing here.')
  next()
})

microserviceKit.init().then(() => {
  server.listen(5001, function() {
    console.log('%s listening at %s', server.name, server.url);
  })
}).catch(err => {
  console.log('error when initialising microservice:')
  console.log(err.stack)
})
