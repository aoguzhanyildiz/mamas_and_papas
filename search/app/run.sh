URL="${ELASTIC_URL:-http://localhost:9200}"
curl -XDELETE "$URL/products?pretty";
curl -s -H "Content-Type: application/x-ndjson" -XPOST $URL/_bulk --data-binary "@products.json";
yarn start;
