const MicroserviceKit = require('microservice-kit')
const elasticsearch = require('elasticsearch');
const AgentKeepAlive = require('agentkeepalive');

const elasticURL = process.env.ELASTIC_URL || 'http://localhost:9200'
const rabbitmqURL = process.env.RABBITMQ_URL || 'amqp://localhost:5672'

// init ES client
const esClient = new elasticsearch.Client({
  host: elasticURL,
  log: 'info'
});

// Check if ES is up
esClient.ping({
  requestTimeout: 1000
}, function (error) {
  if (error) {
    console.error('Elasticsearch Cluster -> Down!');
  } else {
    console.log('Elasticsearch Cluster -> Up!');
  }
});

const queryType = keyword => {
  // if there are more than 3 numbers in keyword,
  // it should be either sku or ediRef.
  // otherwise do full text search with fuzziness
  const shouldExactMatch = keyword.split('').filter(c => !isNaN(c)).length > 3
  if (shouldExactMatch) {
    return {
      multi_match: {
        query: keyword,
        fields: [ "sku", "ediRef" ],
        type: "phrase_prefix",
      }
    }

  } else {
    return {
      bool: {
        should: [
          {
            query_string: {
                fields: [ "name^20", "description" ], // prioritize name field.
                query: `*${keyword}*`
            }
          },
          {
            fuzzy: {
              name: {
                boost: 3.0,
                value: keyword
              }
            }
          }
        ],
        minimum_should_match : "70%", // Played with this a little. 70% seems fine.
        boost : 1.0
      }
    }
  }
}

// construct
const microserviceKit = new MicroserviceKit({
  type: 'core-worker',
  amqp: {
    url: rabbitmqURL,
    queues: [ {
      name: "core",
      options: {
        durable: true
      },
      key: 'core'
    } ],
    exchanges: []
  }
});
microserviceKit.init().then(() => {
  const coreQueue = microserviceKit.amqpKit.getQueue('core');

  coreQueue.consumeEvent('search', (data, callback, progress, routingKey) => {
    esClient.search({
      index: 'products',
      type: 'product',
      body: {
        track_scores: true,
        query: queryType(data.keyword),
        size: 75,
        sort: [ {
          "isInStock": {
            "order": "desc"
          }
        } ]
      }
    }).then(function (resp) {
      callback(null, resp)
    }, function (err) {
      callback(err, null)
      console.trace(err.message);
    });
  });
}).catch(err => {
  console.log('error when initialising microservice:')
  console.log(err.stack)
})
